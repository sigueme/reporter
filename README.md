# Fleety Reporter

Fleety's report maker

## Deploy

* Clone
* copy settings.py to settings_<environment>.py and modify (Don't override settings with default values)
* create virtualenv
* install requirements
* export settings to environment variable REPORTER_SETTINGS
* run `python main.py`
