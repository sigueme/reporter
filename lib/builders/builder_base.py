from case_conversion import snakecase
from datetime import date
from random import choice
from tempfile import TemporaryFile
import boto3
import psycopg2
import json


class Builder:

    def __init__(self, config, report_data):
        self.config      = config
        self.report_data = report_data

    def build(self):
        # Get the data
        data = self.get_data()

        # Write the data to a temporary file
        with TemporaryFile(mode='w+b') as tmpf:
            self.get_file(tmpf, data)
            tmpf.seek(0)

            # Upload it to s3
            filename = self.get_name()
            url = self.publish(tmpf, filename)

        return url

    def get_conn(self, db):
        return psycopg2.connect(
            dbname   = db,
            user     = self.config['POSTGRES_USER'],
            password = self.config['POSTGRES_PASSWORD'],
            host     = self.config['POSTGRES_HOST'],
            port     = self.config['POSTGRES_PORT'],
        )

    def get_data(self):
        ''' Must query the database and return the data needed for this report '''
        raise NotImplementedError()

    def get_file(self, data):
        ''' Turns the data into a file-like object that will be published '''
        raise NotImplementedError()

    def get_hash(self):
        ''' Returns a random hash for reports '''
        if self.config['TESTING']:
            return 'abc123'

        alphabet = '0123456789abcdef'

        return ''.join(choice(alphabet) for _ in range(6))

    def get_extension(self):
        ''' Returns the file extension that will be used for this report '''
        raise NotImplementedError()

    def get_mime(self):
        ''' returns the expected mime type of this report when downloading '''
        raise NotImplementedError()

    def get_name(self):
        ''' Returns the filename that will be given to this report '''
        return '{org_subdomain}/reports/{report_class}/{iso_date}_{hash}.{extension}'.format(
            org_subdomain = self.report_data['_type'].split(':')[0],
            report_class  = '_'.join(snakecase(type(self).__name__, detect_acronyms=True, acronyms=self.config['ACRONYMS']).split('_')[:-1]),
            iso_date      = date.today().isoformat(),
            hash          = self.get_hash(),
            extension     = self.get_extension(),
        )

    def publish(self, fp, key):
        ''' Publishes this report to the storage '''
        s3 = boto3.resource('s3')

        s3.Object(self.config['S3_BUCKET'], key).put(Body=fp)

        if self.report_data['name']:
            filename = '{}.{}'.format(
                self.report_data['name'],
                self.get_extension(),
            )
        else:
            filename = '{}_{}.{}'.format(
                type(self).__name__,
                date.today().isoformat(),
                self.get_extension(),
            )

        s3_client = boto3.client('s3')
        url = s3_client.generate_presigned_url('get_object', Params = {
            'Bucket': self.config['S3_BUCKET'],
            'Key': key,
            'ResponseContentType': self.get_mime(),
            'ResponseContentDisposition': 'attachment; filename="{}"'.format(filename),
        }, ExpiresIn = self.config['S3_URL_EXPIRATION'])

        return url


class CSVBuilder(Builder):

    def get_extension(self):
        return 'csv'

    def get_mime(self):
        return 'text/csv'

    def get_file(self, tmpf, data):
        tmpf.write(self.get_header())

        for row in data:
            tmpf.write(self.get_row(row))

    # now a bunch of useful commands for processing database output
    def _str(self, data):
        if data is None:
            return b''

        if ',' in data:
            data = '"{}"'.format(data)

        return data.encode('utf8')

    def _int(self, data):
        if data is None:
            return b''

        return str(data).encode('utf8')

    def _datetime(self, data):
        if data is None:
            return b''

        return (data.isoformat() + 'Z').encode('utf8')

    def _point(self, data):
        if data is None:
            return b''

        return ('"' + ','.join(data[6:-1].split()) + '"').encode('utf8')


class GeoJSONBuilder(Builder):

    def get_extension(self):
        return 'geojson'

    def get_mime(self):
        return 'application/json'

    def get_lon_lat(self, item):
        ''' This method of the builder should return a list of [longitude, latitude]
        for each item returned by data '''
        raise NotImplementedError()

    def get_file(self, tmpf, data):
        coordinates = []

        for item in data:
            coordinates.append(self.get_lon_lat(item))

        json_data = {
            'type': 'FeatureCollection',
            'features': [{
                'type': 'Feature',
                'properties': {},
                'geometry': {
                    'type': 'LineString',
                    'coordinates': coordinates,
                },
            }],
        }

        tmpf.write(json.dumps(json_data).encode('utf8'))
