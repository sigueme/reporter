import psycopg2.extras

from .builder_base import GeoJSONBuilder


class DeviceGeoJSONBuilder(GeoJSONBuilder):

    def get_data(self):
        conn = self.get_conn(self.config['POSTGRES_DB_TRACCAR'])
        cur = conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)

        cur.execute('''SELECT *
            FROM positions
            WHERE
                deviceid=(SELECT id FROM devices WHERE uniqueid=%s) AND
                devicetime BETWEEN %s AND %s
            ;''', (
            self.report_data['params']['device_code'],
            self.report_data['params']['export_from'],
            self.report_data['params']['export_to'],
        ))

        data = cur.fetchall()

        cur.close()
        conn.close()

        return data

    def get_lon_lat(self, item):
        return [
            float(item['longitude']),
            float(item['latitude']),
        ]
