import psycopg2.extras

from .builder_base import CSVBuilder


class TripCSVBuilder(CSVBuilder):

    def get_header(self):
        return b','.join([
            b'id',
            b'status',
            b'origin',
            b'origin_pos',
            b'destination',
            b'destination_pos',
            b'user_id',
            b'route_id',
            b'fleet_id',
            b'scheduled_departure',
            b'scheduled_arrival',
            b'notes',
            b'device_id',
            b'departure',
            b'arrival',
        ]) + b'\n'

    def get_row(self, row):
        return b','.join([
            self._int(row['id']),
            self._str(row['status']),
            self._str(row['origin']),
            self._point(row['origin_pos']),
            self._str(row['destination']),
            self._point(row['destination_pos']),
            self._str(row['user_id']),
            self._str(row['route_id']),
            self._str(row['fleet_id']),
            self._datetime(row['scheduled_departure']),
            self._datetime(row['scheduled_arrival']),
            self._str(row['notes']),
            self._str(row['device_id']),
            self._datetime(row['departure']),
            self._datetime(row['arrival']),
        ]) + b'\n'

    def get_data(self):
        conn = self.get_conn(self.config['POSTGRES_DB_FLEETY'])
        cur = conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)

        cur.execute('''SELECT
            id,
            status,
            origin,
            ST_AsText(origin_pos) as origin_pos,
            destination,
            ST_AsText(destination_pos) as destination_pos,
            user_id,
            route_id,
            fleet_id,
            scheduled_departure,
            scheduled_arrival,
            notes,
            device_id,
            departure,
            arrival
        FROM trip
        WHERE org_subdomain=%s
        ORDER BY id ASC''', (self.report_data['_type'].split(':')[0],))
        data = cur.fetchall()

        cur.close()
        conn.close()

        return data
