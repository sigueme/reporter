from .logger import log
from .main_handler import MainHandler
from multiprocessing import Pool
import redis
import signal

# https://stackoverflow.com/questions/27745842/redis-pubsub-and-message-queueing
# https://stackoverflow.com/questions/1408356/keyboard-interrupts-with-pythons-multiprocessing-pool#1408476

def init_worker():
    signal.signal(signal.SIGINT, signal.SIG_IGN)


class Loop:

    def __init__(self, config):
        self.redis = redis.StrictRedis.from_url(
            config.REDIS_URL,
            decode_responses = True,
        )
        self.pool = Pool(config.WORKERS, init_worker)
        self.config = config
        self.handler = MainHandler(config)

        log.info('Initialized loop')

    def start(self):
        log.info('Running loop')

        ps = self.redis.pubsub()

        log.info('Subscribing to pattern: "{}"'.format(self.config.CHANNEL_PATTERN))
        ps.psubscribe(self.config.CHANNEL_PATTERN)

        with self.pool as pool:
            while True:
                try:
                    message = ps.get_message(timeout=1.0)

                    if message:
                        pool.apply_async(self.handler,
                            args           = [message],
                        )

                except KeyboardInterrupt as e:
                    break

        log.info('Stopped loop')
