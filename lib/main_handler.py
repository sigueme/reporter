from case_conversion import snakecase
from datetime import datetime
from importlib import import_module
import redis
import simplejson as json
import psycopg2

from .logger import log


class MainHandler:

    # this function runs in a different process...
    def __init__(self, config):
        self.config = config.copy()
        self.builders = dict()
        self.redis = None

    # ...than this one, so no conexion can be shared between the two
    def __call__(self, event):
        try:
            self.call(event)
        except Exception as error:
            log.exception('{} {}'.format(type(error).__name__, error))

    def call(self, event):
        parsed_event = self.parse_event(event)

        if parsed_event is None:
            return

        # Only listen to specific events
        if parsed_event['event'] not in self.config['EVENTS']:
            return

        # process report
        builder = self.get_builder(
            parsed_event['data']['report'],
        )
        log.info('Building {} from channel {}'.format(
            type(builder).__name__,
            parsed_event['channel'],
        ))
        try:
            url = builder.build()

            # update database and notify
            completed_at = datetime.now()
            status = 'COMPLETED'
            failed_reason = None

            log.info('Built {} from channel {}'.format(
                type(builder).__name__,
                parsed_event['channel'],
            ))
        except Exception as e:
            completed_at = None
            status = 'FAILED'
            failed_reason = '{}: {}'.format(type(e).__name__, str(e))
            url = None

            log.warning('FAILED {} from channel {}'.format(
                type(builder).__name__,
                parsed_event['channel'],
            ))

        self.update_report(
            parsed_event['data']['report'],
            status,
            completed_at,
            failed_reason,
            url,
        )
        self.notify_finished(
            parsed_event['channel'],
            parsed_event['data']['report'],
            status,
            completed_at,
            failed_reason,
            url,
        )

    def get_builder(self, report_data):
        module = import_module(
            '.builders.{}_builder'.format(
                snakecase(report_data['builder'],
                    detect_acronyms=True,
                    acronyms=self.config['ACRONYMS'],
                )
            ),
            'lib'
        )

        return getattr(module, report_data['builder']+'Builder')(
            self.config,
            report_data,
        )

    def parse_event(self, event):
        if event['type'] != 'pmessage':
            # it's not even a message... discard
            return

        data = event['data']

        try:
            data = json.loads(data)
        except json.decoder.JSONDecodeError as e:
            log.warning('Couldn\'t decode event\'s JSON data:')
            log.warning(event)
            return

        if 'event' not in data:
            log.warning('Received event without event field')
            log.warning(event)
            return

        if 'data' not in data:
            log.warning('Received event without data field')
            log.warning(event)
            return

        channel = event['channel']

        return {
            'data': data['data'],
            'event': data['event'],
            'channel': channel,
            'org': channel.split(':')[0],
        }

    def get_redis(self):
        if not self.redis:
            self.redis = redis.StrictRedis.from_url(
                self.config['REDIS_URL'],
                decode_responses = True,
            )

        return self.redis

    def get_fleety_conn(self):
        return psycopg2.connect(
            dbname   = self.config['POSTGRES_DB_FLEETY'],
            user     = self.config['POSTGRES_USER'],
            password = self.config['POSTGRES_PASSWORD'],
            host     = self.config['POSTGRES_HOST'],
            port     = self.config['POSTGRES_PORT'],
        )

    def update_report(self, report, status, completed_at, failed_reason, url):
        ''' updates this report in the database '''
        conn = self.get_fleety_conn()
        cur = conn.cursor()

        cur.execute('UPDATE report SET status=%s, completed_at=%s, failed_reason=%s, url=%s WHERE id=%s', (
            status,
            completed_at,
            failed_reason,
            url,
            report['id'],
        ))
        conn.commit()
        cur.close()
        conn.close()

    def notify_finished(self, channel, report, status, completed_at, failed_reason, url):
        ''' this method triggers the report-finished event '''
        redis = self.get_redis()

        if completed_at is not None:
            report['completed_at'] = completed_at.replace(microsecond=0).isoformat()+'Z'
        report['status'] = status
        report['failed_reason'] = failed_reason
        report['url'] = url

        redis.publish(channel, json.dumps({
            'event': 'report-' + ('finished' if status == 'COMPLETED' else 'failed'),
            'data': {
                'report': report,
                'org_name': report['_type'].split(':')[0],
            },
        }))
