# Is this a testing environment?
TESTING = False

# Redis connection
REDIS_URL = 'redis://localhost:6379/0'

# Process management
WORKERS = 4

# Channels
CHANNEL_PATTERN = '*:user:*'

# Events
EVENTS = ['report-requested']

# Postgres connection params
POSTGRES_DB_FLEETY = 'fleety'
POSTGRES_DB_TRACCAR = 'traccar'
POSTGRES_USER = None
POSTGRES_PASSWORD = None
POSTGRES_HOST = 'localhost'
POSTGRES_PORT = 5432

# Time settings
TIMEZONE = 'UTC'

# Used for name transformation between CamelCase and snake_case
ACRONYMS = [
    'CSV',
    'PDF',
    'JSON',
    'GPX',
]

# For error reporting via the broker
FLEETY_ERROR_CHANNEL = 'meta:server:fleety-reporter'

# For template rendering
URL_PROTOCOL = 'https'
URL_SUBDOMAIN = 'com'

# For google static maps
GOOGLE_API_STATIC_MAPS_KEY = ''

# Amazon AWS
S3_BUCKET = 'fleety'
# six months        m   d    H    M    S
S3_URL_EXPIRATION = 6 * 30 * 24 * 60 * 60
