import boto3
import os
import psycopg2
import time
import unittest
import redis

from lib.config import Config


class ReporterTest(unittest.TestCase):

    def load_db(self, db):
        return psycopg2.connect(
            dbname   = db,
            user     = self.config['POSTGRES_USER'],
            password = self.config['POSTGRES_PASSWORD'],
            host     = self.config['POSTGRES_HOST'],
            port     = self.config['POSTGRES_PORT'],
        )

    def load_config(self):
        config = Config(os.path.join(os.path.dirname(os.path.realpath(__file__)), '..'))
        config.from_pyfile('settings.py')
        config.from_pyfile('settings_testing.py', silent=True)

        return config

    def load_redis(self):
        return redis.StrictRedis.from_url(self.config.REDIS_URL, decode_responses=True)

    def load_s3(self):
        return boto3.resource('s3').Bucket(self.config.S3_BUCKET)

    def load_time(self):
        os.environ['TZ'] = self.config.TIMEZONE
        time.tzset()
