from datetime import datetime, date
from tempfile import TemporaryFile
import psycopg2
import unittest
import json

from lib.builders.trip_csv_builder import TripCSVBuilder
from lib.builders.device_geo_json_builder import DeviceGeoJSONBuilder
from . import ReporterTest


class BuilderTest(ReporterTest):

    def setUp(self):
        super().setUp()

        self.config = self.load_config()
        self.load_time()

        self.maxDiff = None

    def test_get_name(self):
        builder = TripCSVBuilder(self.config, {
            '_type': 'anorg',
        })

        parts = builder.get_name().split('/')

        # {org}/reports/{report}/{isodate}_{hash}.{ext}

        self.assertEqual(parts[0], 'anorg')
        self.assertEqual(parts[1], 'reports')
        self.assertEqual(parts[2], 'trip_csv')

        self.assertTrue(parts[3].startswith(date.today().isoformat()))
        self.assertTrue(parts[3].endswith('.csv'))

    def test_trip_csv(self):
        ''' TripCSVBuilder '''
        conn = self.load_db(self.config.POSTGRES_DB_FLEETY)
        cur = conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)

        cur.execute('DELETE FROM trip;')
        cur.execute('ALTER SEQUENCE trip_id_seq RESTART WITH 1;')

        departure = datetime(2018, 1, 25, 15, 15)
        arrival = datetime(2018, 1, 25, 19, 30)

        trips = [
            ('testing', 'FINISHED', 'Guatemala', 'POINT(-90.67016 15.09864)', 'Mexico', 'POINT(-101.49169 21.86149)', 'anuser', None, 'afleet', departure, arrival, '', 'adevice', departure, arrival),
            ('testing', 'FINISHED', 'Guatemala', 'POINT(-90.67016 15.09864)', 'Mexico', 'POINT(-101.49169 21.86149)', 'anuser', None, 'afleet', departure, arrival, '', 'adevice', departure, arrival),
            ('other', 'FINISHED', 'Guatemala', 'POINT(-90.67016 15.09864)', 'Mexico', 'POINT(-101.49169 21.86149)', 'anuser', None, 'afleet', departure, arrival, '', 'adevice', departure, arrival),
            ('other', 'FINISHED', 'Guatemala', 'POINT(-90.67016 15.09864)', 'Mexico', 'POINT(-101.49169 21.86149)', 'anuser', None, 'afleet', departure, arrival, '', 'adevice', departure, arrival),
        ]

        for item in trips:
            cur.execute('INSERT INTO trip (org_subdomain, status, origin, origin_pos, destination, destination_pos, user_id, route_id, fleet_id, scheduled_departure, scheduled_arrival, notes, device_id, departure, arrival) values (%s, %s, %s, ST_GeomFromText(%s), %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s);', item)
        conn.commit()

        cur.close()
        conn.close()

        REPORT_KEY = 'testing/reports/trip_csv/{}_abc123.csv'.format(date.today().isoformat())
        report = {
            '_type': 'testing:report',
            'name': None,
        }
        builder = TripCSVBuilder(
            self.config,
            report,
        )

        builder.build()

        exp = '''id,status,origin,origin_pos,destination,destination_pos,user_id,route_id,fleet_id,scheduled_departure,scheduled_arrival,notes,device_id,departure,arrival
1,FINISHED,Guatemala,"-90.67016,15.09864",Mexico,"-101.49169,21.86149",anuser,,afleet,{departure},{arrival},,adevice,{departure},{arrival}
2,FINISHED,Guatemala,"-90.67016,15.09864",Mexico,"-101.49169,21.86149",anuser,,afleet,{departure},{arrival},,adevice,{departure},{arrival}
'''.format(
    departure = departure.replace(microsecond=0).isoformat()+'Z',
    arrival = arrival.replace(microsecond=0).isoformat()+'Z',
).encode('utf8')

        with TemporaryFile() as fp:
            self.load_s3().download_fileobj(REPORT_KEY, fp)
            fp.seek(0)

            for line1, line2 in zip(fp, exp.splitlines(keepends=True)):
                self.assertEqual(line1, line2)

    def test_device_geojson(self):
        ''' DeviceGeoJSONBuilder '''
        conn = self.load_db(self.config.POSTGRES_DB_TRACCAR)
        cur = conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)

        cur.execute('DELETE FROM devices;')
        cur.execute('ALTER SEQUENCE devices_id_seq RESTART WITH 1;')

        devices = [
            ('dev1', '1234567800001'),
            ('dev2', '1234567800002'),
        ]

        for item in devices:
            cur.execute('INSERT INTO devices (name, uniqueid) VALUES (%s, %s)', item)

        cur.execute('DELETE FROM positions;')
        cur.execute('ALTER SEQUENCE positions_id_seq RESTART WITH 1;')

        positions = [
            (1, -103.41238, 20.31243, datetime(2018, 1, 30, 12, 12), datetime(2018, 1, 30, 12, 12), datetime(2018, 1, 30, 12, 12), True, 0, 0, 0), # not in report
            (1, -103.35525, 20.74198, datetime(2018, 1, 30, 12, 31), datetime(2018, 1, 30, 12, 31), datetime(2018, 1, 30, 12, 31), True, 0, 0, 0),
            (1, -102.99545, 20.74455, datetime(2018, 1, 30, 12, 32), datetime(2018, 1, 30, 12, 32), datetime(2018, 1, 30, 12, 32), True, 0, 0, 0),
            (1, -102.79907, 20.93835, datetime(2018, 1, 30, 12, 33), datetime(2018, 1, 30, 12, 33), datetime(2018, 1, 30, 12, 33), True, 0, 0, 0),
            (1, -102.51480, 20.97682, datetime(2018, 1, 30, 12, 34), datetime(2018, 1, 30, 12, 34), datetime(2018, 1, 30, 12, 34), True, 0, 0, 0),
            (2, -102.98243, 20.57346, datetime(2018, 1, 30, 12, 45), datetime(2018, 1, 30, 12, 45), datetime(2018, 1, 30, 12, 45), True, 0, 0, 0), # not in report
            (2, -102.23438, 20.15434, datetime(2018, 1, 30, 12, 46), datetime(2018, 1, 30, 12, 46), datetime(2018, 1, 30, 12, 46), True, 0, 0, 0), # not in report
        ]

        for item in positions:
            cur.execute('INSERT INTO positions (deviceid, longitude, latitude, servertime, devicetime, fixtime, valid, altitude, speed, course) values (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s);', item)
        conn.commit()

        cur.close()
        conn.close()

        REPORT_KEY = 'testing/reports/device_geo_json/{}_abc123.geojson'.format(date.today().isoformat())
        export_from = datetime(2018, 1, 30, 12, 30)
        export_to = datetime(2018, 1, 30, 13)

        report = {
            '_type': 'testing:report',
            'name': None,
            'params': {
                'device_code': '1234567800001',
                'export_from': export_from.replace(microsecond=0).isoformat()+'Z',
                'export_to': export_to.replace(microsecond=0).isoformat()+'Z',
            },
        }
        builder = DeviceGeoJSONBuilder(
            self.config,
            report,
        )

        builder.build()

        with TemporaryFile() as fp:
            print(REPORT_KEY)
            self.load_s3().download_fileobj(REPORT_KEY, fp)
            fp.seek(0)

            data = json.load(fp)

            self.assertDictEqual(data, {
              "type": "FeatureCollection",
              "features": [
                {
                  "type": "Feature",
                  "properties": {},
                  "geometry": {
                    "type": "LineString",
                    "coordinates": [
                      [
                        -103.35525,
                        20.74198
                      ],
                      [
                        -102.99545,
                        20.74455
                      ],
                      [
                        -102.79907,
                        20.93835
                      ],
                      [
                        -102.51480,
                        20.97682
                      ]
                    ]
                  }
                }
              ]
            })
