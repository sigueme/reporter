from datetime import datetime, date
from subprocess import Popen
from tempfile import TemporaryFile
import boto3
import botocore
import os
import psycopg2.extras
import simplejson as json
import unittest
import os

from lib.loop import Loop
from . import ReporterTest


class CompleteTest(ReporterTest):
    ''' A single test that tests the whole process '''

    def load_loop(self):
        settings_filename = os.path.join(os.path.dirname(os.path.abspath(__file__)), '../settings_testing.py')
        return Popen('REPORTER_SETTINGS={} python main.py'.format(settings_filename), shell=True)

    def setUp(self):
        super().setUp()

        self.config = self.load_config()
        self.conn = self.load_db(self.config.POSTGRES_DB_FLEETY)
        self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
        self.load_time()
        self.loop = self.load_loop()

        self.cur.execute('DELETE FROM report;')
        self.cur.execute('ALTER SEQUENCE report_id_seq RESTART WITH 1;')

        self.cur.execute('DELETE FROM trip;')
        self.cur.execute('ALTER SEQUENCE trip_id_seq RESTART WITH 1;')

        self.departure = datetime(2018, 1, 26, 14, 24)
        self.arrival = datetime(2018, 1, 26, 17, 30)

        self.cur.execute('INSERT INTO trip (org_subdomain, status, origin, origin_pos, destination, destination_pos, user_id, route_id, fleet_id, scheduled_departure, scheduled_arrival, notes, device_id, departure, arrival) values (%s, %s, %s, ST_GeomFromText(%s), %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s);', ('testing', 'FINISHED', 'Guatemala', 'POINT(-90.67016 15.09864)', 'Mexico', 'POINT(-101.49169 21.86149)', 'anuser', None, 'afleet', self.departure, self.arrival, '', 'adevice', self.departure, self.arrival))

        self.conn.commit()
        self.maxDiff = None

    def tearDown(self):
        self.cur.close()
        self.conn.close()
        self.loop.terminate()

    def test_can_generate_simple_report(self):
        # some data needed during the test
        id            = 1
        scheduler_id  = None
        org_subdomain = 'testing'
        status        = 'IN_PROGRESS'
        builder       = 'TripCSV'
        params        = '{}'
        requested_at  = datetime(2018, 1, 24, 16, 30)
        completed_at  = None
        name          = 'My test trip report'
        user_id       = 'anadmin'
        REPORT_KEY    = 'testing/reports/trip_csv/{}_abc123.csv'.format(date.today().isoformat())
        EXPECTED_TIME = 5.0 # Seconds that this test is expected to last

        # check that report wasn't in s3 from the begining
        try:
            self.load_s3().Object(REPORT_KEY).delete()
        except botocore.exceptions.ClientError:
            pass

        # seed a report
        self.cur.execute('INSERT INTO report (scheduler_id, org_subdomain, status, builder, params, requested_at, completed_at, name, user_id) values (%s, %s, %s, %s, %s, %s, %s, %s, %s);', (
            scheduler_id,
            org_subdomain,
            status,
            builder,
            params,
            requested_at,
            completed_at,
            name,
            user_id,
        ))
        self.conn.commit()

        # trigger the event
        r = self.load_redis()
        r.publish('testing:user:anadmin', json.dumps({
            'event': 'report-requested',
            'data': {
                'report': {
                    '_type'         : 'testing:report',
                    'id'            : id,
                    'scheduler_id'  : scheduler_id,
                    'org_subdomain' : org_subdomain,
                    'status'        : status,
                    'builder'       : builder,
                    'params'        : params,
                    'requested_at'  : requested_at.replace(microsecond=0).isoformat() + 'Z',
                    'completed_at'  : completed_at,
                    'name'          : name,
                    'user_id'       : user_id,
                },
                'org_name': 'testing',
            },
        }))

        # check that report-finished event was triggered
        ps = r.pubsub()
        ps.subscribe('testing:user:anadmin')

        msg = ps.get_message() # the subscription message
        self.assertEqual(msg['type'], 'subscribe')

        msg = ps.get_message(timeout=EXPECTED_TIME)
        decoded_msg = json.loads(msg['data'])

        self.assertTrue((datetime.strptime(
            decoded_msg['data']['report']['completed_at'],
            '%Y-%m-%dT%H:%M:%SZ'
        ) - datetime.now()).total_seconds() < EXPECTED_TIME)
        self.assertIsNotNone(decoded_msg['data']['report']['url'])

        del decoded_msg['data']['report']['completed_at']
        del decoded_msg['data']['report']['url']

        self.assertDictEqual(decoded_msg, {
            'event': 'report-finished',
            'data': {
                'report': {
                    '_type'         : 'testing:report',
                    'id'            : id,
                    'scheduler_id'  : scheduler_id,
                    'org_subdomain' : org_subdomain,
                    'status'        : 'COMPLETED',
                    'builder'       : builder,
                    'params'        : params,
                    'requested_at'  : requested_at.replace(microsecond=0).isoformat()+'Z',
                    'name'          : name,
                    'failed_reason' : None,
                    'user_id'       : user_id,
                },
                'org_name': 'testing',
            },
        })

        # check that report attributes have changed
        self.cur.execute('SELECT * FROM report WHERE id=%s;', (1,))
        updated_report = self.cur.fetchone()
        self.assertTrue((updated_report['completed_at'] - datetime.now()).total_seconds() < EXPECTED_TIME)
        self.assertEqual(updated_report['status'], 'COMPLETED')

        # check that report exists in s3 bucket
        exp = '''id,status,origin,origin_pos,destination,destination_pos,user_id,route_id,fleet_id,scheduled_departure,scheduled_arrival,notes,device_id,departure,arrival
1,FINISHED,Guatemala,"-90.67016,15.09864",Mexico,"-101.49169,21.86149",anuser,,afleet,{departure},{arrival},,adevice,{departure},{arrival}
'''.format(
    departure = self.departure.replace(microsecond=0).isoformat()+'Z',
    arrival = self.arrival.replace(microsecond=0).isoformat()+'Z',
).encode('utf8')

        with TemporaryFile() as fp:
            self.load_s3().download_fileobj(REPORT_KEY, fp)
            fp.seek(0)

            for line1, line2 in zip(fp, exp.splitlines(keepends=True)):
                self.assertEqual(line1, line2)


if __name__ == '__main__':
    unittest.main()
